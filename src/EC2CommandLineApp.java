import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.AttachVolumeRequest;
import com.amazonaws.services.ec2.model.CreateVolumeRequest;
import com.amazonaws.services.ec2.model.CreateVolumeResult;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.DescribeVolumesResult;
import com.amazonaws.services.ec2.model.DetachVolumeRequest;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.InstanceState;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.Volume;
import com.amazonaws.services.ec2.model.VolumeType;
import com.amazonaws.services.opsworks.model.StopInstanceRequest;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.simpledb.AmazonSimpleDB;
import com.amazonaws.services.simpledb.AmazonSimpleDBClient;





public class EC2CommandLineApp {
	
	
	//===========================================================================
	//	=>	AWS OBJECTS
	//===========================================================================
	static AmazonEC2      ec2;
	static AmazonS3       s3;
	static AmazonSimpleDB sdb;
	static int counter = 3;
	
	
	
	//===========================================================================
  	//	=>	MAIN METHOD
  	//===========================================================================
	 public static void main(String[] args) {
		 
		 
		try { init(); } 
		catch (Exception e) { 
				System.out.println("Credentials file not found\nPlease create or update a proprerty file at the root of your classpath"); 
				System.exit(1);
		}
		 
		System.out.println("Please input a number to use for devices, this number should be the largest device i.e. if /dev/sda4 is the largest, typing 5 works");
		System.out.println("FOR GRADING PURPOSES TYPE 4");
		Scanner seedReader = new Scanner (System.in);
		EC2CommandLineApp.counter = seedReader.nextInt();
		
		
		while (true) {
			//			UI
			System.out.println("Navigate this AWS command line app by choosing a number");
			System.out.println("***This app does not handle exceptions. If you do not have available instances or volumes for the operation selected, the app will crash***\n");
			System.out.println("1 - List Instances\n");
			System.out.println("2 - List attached volumes\n");
			System.out.println("3 - List unattached volumes\n");
			System.out.println("4 - Attach a volume to an instance\n");
			System.out.println("5 - Detach a volume from an instance\n");
			System.out.println("9 - Exit");
					
			//	get user input
			Scanner s = new Scanner(System.in);
			String rawInput = s.nextLine();
			int parsedInput = 0;
			try { parsedInput = Integer.parseInt(rawInput); } 
			catch (NumberFormatException e) { System.out.println("Please enter a valid number\n"); };
			
			//			handle user input
			switch (parsedInput) {
					case 1:
						listInstances();
						break;
						
					case 2:
						getAttachedVolumes();
						break;
						
					case 3:
						getUnattachedVolumes();
						break;
						
					case 4:
						Scanner scn = new Scanner(System.in);
					    System.out.println("Enter an instance id");
					    String instance = scn.nextLine();
					     
					    System.out.println("Enter the volume id to attach");
					    String volumeId = scn.nextLine();
					    attachVolumeToInstance(volumeId, instance);
						break;
						
					case 5:
						Scanner scn2 = new Scanner(System.in);
					    System.out.println("Enter the volume id to detach");
					    String v = scn2.nextLine();
					    detachVolumeFromInstance(v);
						break;
						
					case 9:
						System.out.println("Good bye");
						System.exit(1);
						break;

					default:
						//	do nothing, continue in while loop
						break;
					}
		}
		
		
	     
	     
	     
	     
	     
	     
	     
	     
		 
        
	     
	     
		 
	 }
     
	 
	 
	 
	 
	//===========================================================================
	//	=>	CONSOLE TRIGGERED METHODS
	//===========================================================================
	 private static void listInstances() {
		 
		 System.out.println("List instances");
         DescribeInstancesResult describeInstancesRequest = ec2.describeInstances();
         List<Reservation> reservations = describeInstancesRequest.getReservations();
         Set<Instance> instances = new HashSet<Instance>();
         // add all instances to a Set.
         for (Reservation reservation : reservations) {
          instances.addAll(reservation.getInstances());
         }
         
         System.out.println("You have " + instances.size() + " Amazon EC2 instance(s).");
         for (Instance ins : instances){
         
          // instance id
          String instanceId = ins.getInstanceId();
         
          // instance state
          InstanceState is = ins.getState();
          System.out.println(instanceId+" "+is.getName());
         }
		 
	 }
	 
	 
	 private static void getAttachedVolumes() {
		 
		 
		 Set<Volume> volumeSet = getVolumes();
         
		 int count = 0;
         for (Volume vol : volumeSet){
         
        	 // instance id
        	 String instanceId = vol.getVolumeId();
        	 String vt = vol.getVolumeType();
        	 String vs = vol.getState();
        	 
        	 if (vs.equals("in-use")) {
        		 System.out.println(instanceId+" "+vt+": "+vs);
        		 count++;
        	 }
        	 
         }
         
         
         System.out.println("You have " + count + " attached volume(s).");
		 
		 
	 }
	 
	 
	 private static void getUnattachedVolumes() {
		 
		 
		 Set<Volume> volumeSet = getVolumes();
         
		 int count = 0;
         for (Volume vol : volumeSet){
         
        	 // instance id
        	 String instanceId = vol.getVolumeId();
        	 String vt = vol.getVolumeType();
        	 String vs = vol.getState();
        	 
        	 if (vs.equals("available")) {
        		 System.out.println(instanceId+" "+vt+": "+vs);
        		 count++;
        	 }
        	 
         }
         
         
         System.out.println("You have " + count + " unattached volume(s).");
		 
		 
	 }
	 
	 
	 private static void attachVolumeToInstance(String volumeId, String instanceId) {
		 
		 
		 Set<Volume> volumes = getVolumes();
		 Volume myVolume = new Volume();
		 for (Volume vol : volumes) {
			 if (vol.getVolumeId().equals(volumeId)) {
				 myVolume = vol;
				 break;
			 }
		 }
		 
         String volumeID = myVolume.getVolumeId();
         AttachVolumeRequest avr = new AttachVolumeRequest();//begin to attach the volume to instance
         avr.withInstanceId(instanceId);
         avr.withVolumeId(volumeID);
         avr.withDevice("/dev/sda"+EC2CommandLineApp.getCounter()); //mount it
         ec2.attachVolume(avr);
         System.out.println("EBS volume has been attached and the volume ID is: "+volumeID);
	 }
	 
	 
	 private static void detachVolumeFromInstance(String volume) {
		
		 ec2.detachVolume(new DetachVolumeRequest(volume));
		 System.out.println("Volume detached");
	 }
	 
	 
	 
	//===========================================================================
	//	=>	PRIVATE HELPERS
	//===========================================================================
	private static void init() throws Exception {
		/*
	 	 * This credentials provider implementation loads your AWS credentials
	 	 * from a properties file at the root of your classpath.
	 	 */
		AWSCredentialsProvider credentialsProvider = new ClasspathPropertiesFileCredentialsProvider();

	        ec2 = new AmazonEC2Client(credentialsProvider);
	        s3  = new AmazonS3Client(credentialsProvider);
	        sdb = new AmazonSimpleDBClient(credentialsProvider);
	 }
	
	
	/**
	 * get a set of all volumes
	 * @return
	 */
	private static Set<Volume> getVolumes() {
		
        DescribeVolumesResult describeVolumesRequest = ec2.describeVolumes();
        List<Volume> volumes = describeVolumesRequest.getVolumes();
        Set<Volume> volumeSet = new HashSet<Volume>();
        // add all instances to a Set.
        for (Volume vol : volumes) {
         volumeSet.add(vol);
        }
		
        return volumeSet;
		
	}
	
	
	private static String getCounter() {
		String a = "";
		int x = ++EC2CommandLineApp.counter;
		return a + x;
	}
	

}
